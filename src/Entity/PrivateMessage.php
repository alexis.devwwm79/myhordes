<?php

namespace App\Entity;

use App\Repository\PrivateMessageRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PrivateMessageRepository::class)
 */
class PrivateMessage
{

    const TEMPLATE_CROW_COMPLAINT_ON       = 1;
    const TEMPLATE_CROW_COMPLAINT_OFF      = 2;
    const TEMPLATE_CROW_TERROR             = 3;
    const TEMPLATE_CROW_THEFT              = 4;
    const TEMPLATE_CROW_AGGRESSION_FAIL    = 5;
    const TEMPLATE_CROW_AGGRESSION_SUCCESS = 6;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="text")
     */
    private $text;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date;

    /**
     * @ORM\ManyToOne(targetEntity=PrivateMessageThread::class, inversedBy="messages")
     * @ORM\JoinColumn(nullable=false)
     */
    private $privateMessageThread;

    /**
     * @ORM\ManyToOne(targetEntity=Citizen::class)
     * @ORM\JoinColumn(nullable=true)
     */
    private $owner;

    /**
     * @ORM\Column(type="boolean")
     */
    private $new;

    /**
     * @ORM\ManyToOne(targetEntity=Citizen::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $recipient;

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $items = [];

    /**
     * @ORM\Column(type="integer")
     */
    private $template = 0;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $foreignID;

    public function __construct()
    {
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getText(): ?string
    {
        return $this->text;
    }

    public function setText(string $text): self
    {
        $this->text = $text;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getPrivateMessageThread(): ?PrivateMessageThread
    {
        return $this->privateMessageThread;
    }

    public function setPrivateMessageThread(?PrivateMessageThread $privateMessageThread): self
    {
        $this->privateMessageThread = $privateMessageThread;

        return $this;
    }

    public function getOwner(): ?Citizen
    {
        return $this->owner;
    }

    public function setOwner(?Citizen $owner): self
    {
        $this->owner = $owner;

        return $this;
    }

    public function getNew(): ?bool
    {
        return $this->new;
    }

    public function setNew(bool $new): self
    {
        $this->new = $new;

        return $this;
    }

    public function getRecipient(): ?Citizen
    {
        return $this->recipient;
    }

    public function setRecipient(?Citizen $recipient): self
    {
        $this->recipient = $recipient;

        return $this;
    }

    public function getItems(): ?array
    {
        return $this->items;
    }

    public function setItems(?array $items): self
    {
        $this->items = $items;

        return $this;
    }

    public function getTemplate(): ?int
    {
        return $this->template;
    }

    public function setTemplate(int $template): self
    {
        $this->template = $template;

        return $this;
    }

    public function getForeignID(): ?int
    {
        return $this->foreignID;
    }

    public function setForeignID(?int $foreignID): self
    {
        $this->foreignID = $foreignID;

        return $this;
    }
}
